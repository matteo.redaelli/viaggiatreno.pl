capolinea(CodStazione):- treno(_, CodStazione).

stazione(CodStazione):- stazione(CodStazione, _, _, _).
stazione(CodStazione, NomeStazione):- stazione(CodStazione, NomeStazione, _, _).
stazione_regione(CodStazione, CodRegione):- stazione(CodStazione, _, CodRegione, _).