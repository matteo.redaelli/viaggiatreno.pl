:- module(download,
	  [get_andamento_treno/3,
	   get_stazioni/2]).

:- use_module(viaggiatreno_api).
:- use_module(utils).

:- dynamic(treno_dettagli/3).
:- dynamic(stazione/4).

%% get_stazioni(+CodRegione, ?Stazioni)

get_stazioni(CodRegione, Stazioni):-
	regione(CodRegione),
	viaggiatreno_api:cache_or_get(elenco_stazioni(CodRegione, StazioniDictList)),
	stazioni_parser(StazioniDictList, Stazioni).
	%%utils:dump(stazione).

stazioni_parser([],_).
stazioni_parser([Stazione|StazioniDictList], [stazione(CodStazione, NomeLungo, CodRegione, Stazione)|Stazioni]):-
	CodRegione  = Stazione.get(codReg),
	CodStazione = Stazione.get(codStazione),
	NomeLungo   = Stazione.get(localita).get(nomeLungo),
	%%retractall(stazione(CodStazione, NomeLungo, CodRegione, Stazione)),
	%%assert(stazione(CodStazione, NomeLungo, CodRegione, Stazione)),
	stazioni_parser(StazioniDictList, Stazioni).


get_andamento_treno(CodStazione, CodTreno, treno_dettagli(CodStazione,CodTreno,Data)):-
	viaggiatreno_api:cache_or_get(andamento_treno(CodStazione, CodTreno, Data)).